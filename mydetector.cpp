#include "mydetector.h"
#include <QFile>
#include <opencv2/aruco.hpp>
#include <opencv2/xfeatures2d/cuda.hpp>

void MyDetector::detect(cv::Mat &img, std::vector<KeyPoint> &k, Mat &des)
{
    _detector->detectAndCompute(img,noArray() ,k,des);
}

void MyDetector::run(int dev)
{
    cap = new VideoCapture(dev);
    namedWindow("tryout");
    cap->open(dev);
    match_to = loadCurrencyImage(":/currency/20pounds.jpg");
//    match.convertTo(match_to, CV_32F, 1.0 / 255, 0);
    for(;;)
    {
        Mat match_to_grayscale;
        if(!cap->isOpened())
            break;
        cap->operator >>(frame);
        cvtColor(frame, frame_grayscale, cv::COLOR_BGR2GRAY);
        cvtColor(match_to, match_to_grayscale, cv::COLOR_BGR2GRAY);
//        match_to_grayscale.convertTo(match_to_grayscale_2, CV_32F, 1.0/255, 0);
        detect(frame_grayscale, _keypoints1, _descriptors1);
        detect(match_to_grayscale, _keypoints2, _descriptors2);
        emit send_images_to_match(frame_grayscale, match_to_grayscale, _keypoints1, _keypoints2, _descriptors1, _descriptors2);
        if(waitKey(1) >= 60)
            break;
    }
    waitKey(0);
    cap->release();
    destroyAllWindows();
}

Mat MyDetector::loadCurrencyImage(QString qrc, int flag)
{
    QFile file(qrc);
    Mat m;
    if(file.open(QIODevice::ReadOnly))
    {
        qint64 sz = file.size();
        std::vector<uchar> buf(sz);
        file.read((char*)buf.data(), sz);
        m = imdecode(buf, flag);
    }

    return m;
}

void MyDetector::receive_matched_images(Mat img)
{
    imshow("frame", img);
}
