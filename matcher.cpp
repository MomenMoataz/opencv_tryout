#include "matcher.h"
#include <QDebug>

void Matcher::extractFeatures(Mat img1, Mat img2, std::vector<KeyPoint> k1, std::vector<KeyPoint> k2)
{
    _extractor->compute(img1, k1,_descriptors1);
    _extractor->compute(img2, k2,_descriptors2);
}

void Matcher::match(Mat img1, Mat img2, std::vector<KeyPoint> k1, std::vector<KeyPoint> k2, Mat des1, Mat des2)
{
    std::vector< DMatch > matches;
    _matcher->match(des2, des1, matches);

    double max_dist = 0; double min_dist = 50;

    int maxi = des1.rows;
    if (des2.rows < maxi)
        maxi = des2.rows;

    for( int i = 0; i < des1.rows; i++ )
    {
        double dist = matches[i].distance;
        if (dist < 0.000001) continue;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }

    std::vector< DMatch > good_matches;

    if(matches.size() > 0 && k1.size() > 0 && k2.size() > 0)
    {

        for( int i = 0; i < des1.rows; i++ )
        {
            if(matches[i].distance < 0.000001) continue;
            if(matches[i].distance <= max(2*min_dist, 0.02))
                good_matches.push_back( matches[i]);
        }
//        if(good_matches.size() <= 0) return;
//        for(size_t i = 0 ; i < matches.size() ; i++)
//        {
//            int i1 = good_matches[i].queryIdx;
//            int i2 = good_matches[i].trainIdx;
//            //qDebug() << i1 << "    " << static_cast<int>(k1.size());
//            if(i1 >= static_cast<int>(k1.size()) || i1 < 0) {
//                return;
//                good_matches[i].queryIdx = k1.size() - 1;
//            }

//            if(i2 >= static_cast<int>(k2.size())) {
//                return;
//                good_matches[i].trainIdx = k2.size() - 1;
//            }
//        }
        Mat img_matches;
        if(good_matches.size()>0)
            drawMatches(img2, k2, img1, k1,
              good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

        else
            drawMatches( img2, k2, img1, k1,
              matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

        std::vector<Point2f> obj;
        std::vector<Point2f> scene;
        for( int i = 0; i < good_matches.size(); i++ )
        {
        //-- Get the keypoints from the good matches
        obj.push_back( k1[ good_matches[i].queryIdx ].pt );
        scene.push_back( k2[ good_matches[i].trainIdx ].pt );
        }

        if(obj.size() > 0)
        {
            Mat H = findHomography(obj, scene, CV_RANSAC);


            //-- Get the corners from the image_1 ( the object to be "detected" )
            std::vector<Point2f> obj_corners(4);
            obj_corners[0] = Point2f(0,0);
            obj_corners[1] = Point2f(img2.cols, 0);
            obj_corners[2] = Point2f(img2.cols, img2.rows);
            obj_corners[3] = Point2f(0, img2.rows);
            std::vector<Point2f> scene_corners(4);

            int height = H.size().height;
            int width = H.size().width;

            if(height > 0 && width > 0)
            {

                perspectiveTransform( obj_corners, scene_corners, H);

                //-- Draw lines between the corners (the mapped object in the scene - image_2 )
                line(img_matches, scene_corners[0] , scene_corners[1] , Scalar(0, 255, 0), 4);
                line(img_matches, scene_corners[3] , scene_corners[0], Scalar( 0, 255, 0), 4);
                line(img_matches, scene_corners[1] , scene_corners[2], Scalar( 0, 255, 0), 4);
                line(img_matches, scene_corners[2] , scene_corners[3], Scalar( 0, 255, 0), 4);
//                imshow( "Good Matches & Object detection", img_matches );
            }
         }

         emit send_matched_image(img_matches);
    }
    good_matches.clear();
    matches.clear();
}

void Matcher::receive_images(Mat img1, Mat img2, std::vector<KeyPoint> k1, std::vector<KeyPoint> k2, Mat des1, Mat des2)
{
    match(img1, img2, k1, k2, des1, des2);
}
