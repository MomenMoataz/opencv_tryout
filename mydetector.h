#pragma once
#include <QObject>
#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>
#include "opencv2/core.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"

using namespace cv;

class MyDetector : public QObject
{
    Q_OBJECT
private:
//    FastFeatureDetector _detector;
    Mat _image;
    Mat match, match_to;
    Mat frame_grayscale, frame;
    Mat _descriptors1, _descriptors2;
    VideoCapture *cap;
    std::vector<cv::KeyPoint> _keypoints1, _keypoints2;
    Ptr<xfeatures2d::SurfFeatureDetector> _detector;

public:
    MyDetector() {

        _detector = xfeatures2d::SurfFeatureDetector::create();
        _detector->setHessianThreshold(1000);
    }
    void run(int dev);
    void detect(Mat &img, std::vector<KeyPoint> &k, Mat &des);
    Mat loadCurrencyImage(QString qrc, int flag = IMREAD_COLOR);

signals:
    void send_images_to_match(Mat, Mat, std::vector<KeyPoint>, std::vector<KeyPoint>, Mat, Mat);

public slots:
    void receive_matched_images(Mat);
};
