#pragma once
#include <QObject>
#include <opencv2/opencv.hpp>
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/tracking.hpp>

using namespace cv;

class Matcher : public QObject
{
    Q_OBJECT
public:
    Matcher() {
        _extractor = xfeatures2d::SurfDescriptorExtractor::create();
        _classifier = new CascadeClassifier();
        _matcher = BFMatcher::create(NORM_L2SQR);
    }
    void extractFeatures(Mat img1, Mat img2, std::vector<KeyPoint> k1, std::vector<KeyPoint> k2);
    void match(Mat img1, Mat img2, std::vector<KeyPoint> k1, std::vector<KeyPoint> k2, Mat des1, Mat des2);

private:
    Ptr<xfeatures2d::SurfDescriptorExtractor> _extractor;
    Ptr<BFMatcher> _matcher;
    Mat _descriptors1, _descriptors2;
    CascadeClassifier *_classifier;
signals:
    void send_matched_image(Mat img);

public slots:
    void receive_images(Mat img1, Mat img2, std::vector<KeyPoint> k1, std::vector<KeyPoint> k2, Mat des1, Mat des2);
};
