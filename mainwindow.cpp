#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
//    _detector = new Detector();
//    _matcher = new Matcher();
    ui->setupUi(this);
    connect(&_detector, SIGNAL(send_images_to_match(Mat,Mat,std::vector<KeyPoint>,std::vector<KeyPoint>, Mat, Mat)),
            &_matcher, SLOT(receive_images(Mat,Mat,std::vector<KeyPoint>,std::vector<KeyPoint>, Mat, Mat)));
    connect(&_matcher, SIGNAL(send_matched_image(Mat)), &_detector, SLOT(receive_matched_images(Mat)));
    _detector.run(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}
