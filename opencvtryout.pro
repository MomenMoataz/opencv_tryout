#-------------------------------------------------
#
# Project created by QtCreator 2017-06-16T22:53:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = opencvtryout
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    matcher.cpp \
    mydetector.cpp

HEADERS  += mainwindow.h \
    matcher.h \
    mydetector.h

FORMS    += mainwindow.ui

CONFIG += link_pkgconfig
PKGCONFIG += opencv

DISTFILES += \
    helloworld.py

RESOURCES += \
    currency.qrc
