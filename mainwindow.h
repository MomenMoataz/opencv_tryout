#pragma once
#include <QMainWindow>
#include "mydetector.h"
#include <matcher.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    MyDetector _detector;
    Matcher _matcher;
};
